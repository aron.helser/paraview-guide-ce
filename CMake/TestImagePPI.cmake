set (IMAGE_EXTS "png" "jpg")

set (IMAGE_FILES)
foreach (DIR ${IMAGE_DIRS})
  if (EXISTS ${DIR})
    foreach (EXTENTION ${IMAGE_EXTS})
      file (GLOB IMAGES ${DIR}/*.${EXTENTION})
      list (APPEND IMAGE_FILES ${IMAGES})
    endforeach()
  endif()
endforeach()

foreach (IMAGE ${IMAGE_FILES})
  execute_process(
    COMMAND identify -format "Horizontal:%x-Vertical:%y" -units PixelsPerInch ${IMAGE}
    OUTPUT_VARIABLE IMAGE_PPI_TEXT
  )
  if (NOT ${IMAGE_PPI_TEXT} MATCHES "Horizontal:200.*-Vertical:200.*")
    list (APPEND FAILED_IMAGES ${IMAGE})
  endif()
endforeach()

if (FAILED_IMAGES)
  message ("The following images failed the PPI test: ")
  foreach (IMAGE ${FAILED_IMAGES})
    message (${IMAGE})
  endforeach()
  message ("PPI needs to be 200x200")
endif()
