include(UseLATEX)

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/../ParaView/menukeys.sty ${CMAKE_CURRENT_SOURCE_DIR}/../ParaView/InsightSoftwareGuide.cls ${CMAKE_CURRENT_SOURCE_DIR}/../ParaView/Insight.sty DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

set(name "ParaViewCatalystUsersGuide")
add_latex_document(
  ${name}.tex
  INPUTS
    Acknowledgements.tex
    Introduction.tex
    UserSection.tex
    DeveloperSection.tex
    BuildSection.tex
    Examples.tex
    References.tex
    Appendix.tex
    multicols.sty
  IMAGE_DIRS Images
#  USE_INDEX
)

add_test(
  NAME TestParaViewCatalystGuide
  COMMAND ${CMAKE_COMMAND} 
    -D LOG_FILE_NAME:STRING=${CMAKE_CURRENT_BINARY_DIR}/ParaViewCatalystUsersGuide.log
    -P ${CMAKE_BINARY_DIR}/CMake/TestLatexLog.cmake)
set_tests_properties(TestParaViewCatalystGuide PROPERTIES
  FAIL_REGULAR_EXPRESSION "Error;Warning")

if (false)
add_test(
  NAME TestParaViewCatalystImages
  COMMAND ${CMAKE_COMMAND}
    -D IMAGE_DIRS:STRING=${CMAKE_CURRENT_SOURCE_DIR}/Images
    -P ${CMAKE_SOURCE_DIR}/CMake/TestImagePPI.cmake)
set_tests_properties(TestParaViewCatalystImages PROPERTIES
  FAIL_REGULAR_EXPRESSION "failed the PPI test")
endif()

if (__BUILDBOT_UPLOAD_FILES)
  copy_to_upload(${name}.pdf "ParaViewCatalystGuide-CE-${MOST_RECENT_TAG}.pdf")
endif()
