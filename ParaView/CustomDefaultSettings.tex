The chapter describes how to specify custom default settings for the
properties of sources, readers, filters, representations, and views.
This can be used to specify, for example, the default background color
for new views, whether a gradient background should be used, the
resolution of a sphere source, which data arrays to load from a
particular file format, and the default setting for almost any other
object property.

The same custom defaults are used across all the \ParaView
executables. This means that custom defaults specified in the
\paraview executable are also used as defaults in
\pvpython and \pvbatch, which makes it easier
to set up a visualization with \paraview and use
\pvpython or \pvbatch to generate an animation
from time-series data, for example.

\section{Customizing defaults for properties}

The Properties panel in \paraview has three sections,
\ui{Properties}, \ui{Display}, and \ui{View}. Each section has two
buttons. These buttons are circled in red in
Figure~\ref{fig:SaveRestoreSettingsButtons}. The button with the disk
icon is used to save the current property values in that section that
have been applied with the \ui{Apply} button. Property values that
have been changed but not applied with the \ui{Apply} button will not
be saved as custom default settings.

The button with the circular arrow (or reload icon) is used to restore
any custom property settings for the object to \ParaView's application
defaults. Once you save the current property settings as defaults, those
values will be treated as the defaults from then on until you change
them to another value or reset them. The saved defaults are written to
a configuration file so that they are available when you close and
launch \ParaView again.

\begin{figure}[ht]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/SaveRestoreSettingsButtons.png}
\caption{Buttons for saving and restoring default property values in the \ui{Properties} panel.}
\label{fig:SaveRestoreSettingsButtons}
\end{center}
\end{figure}

You can undo your changes to the default property values by clicking
on the reload button. This will reset the current view property values
to \paraview's application defaults. To fully restore
\paraview's default values, you need to click the save
button again. If you don't, the restored default values will be
applied only to the current object, and new instances of that object
will have the custom default values that were saved the last time you
clicked the save button.

\section{Example: specifying a custom background color}

Suppose you want to change the default background color in the
\ui{Render View}. To do this, scroll down to the \ui{View} section of
the \ui{Properties} panel and click on the combo box that shows the
current background color. Select a new color, and click \ui{OK}. Next,
scroll up to the \ui{View (Render View)} section header and click on
the disk button to the right of the header. This will save the new
background color as the default background color for new views. To see
this, click on the \ui{+} sign next to the tab above the 3D view to
create a new layout. Click on the \ui{Render View} button. A new
render view will be created with the custom background color you just
saved as default.

\section{Configuring default settings with JSON}
\label{sec:ConfiguringDefaultSettingsJSON}

Custom default settings are stored in a text file in the \JSON
format. We recommend to use the user interface
in \paraview to set most default values, but it is
possible to set them by editing the \JSON settings file directly. It is
always a good idea to make a backup copy of a settings file prior to
manual editing.

The \ParaView executables read from and write to a file named
\directory{ParaView-UserSettings.json}, which is located in your home
directory on your computer. On Windows, this file is located at
\directory{\%APPDATA\%/ParaView/ParaView-UserSettings.json}, where the
\texttt{APPDATA} environment variable is usually something like
\directory{C:/Users/USERNAME/AppData/Roaming}, where \texttt{USERNAME} is
your login name. On Unix-like systems, it is located under
\directory{\textasciitilde/.config/ParaView/ParaView-UserSettings.json}. This file will
exist if you have made any default settings changes through the user
interface in the \paraview executable. Once set, these
default settings will be available in subsequent versions of \ParaView.

A simple example of a file that specifies custom default settings is
shown below:

\begin{python}
{
  "sources" : {
    "SphereSource" : {
      "Radius" : 3.5,
      "ThetaResolution" : 32
    },
    "CylinderSource" : {
      "Radius" : 2
    }
  },
  "views" : {
    "RenderView" : {
      "Background" : [0.0, 0.0, 0.0]
    }
  }
}
\end{python}

Note the hierarchical organization of the file. The first level of the
hierarchy specifies the group to which the object whose settings are
being specified refers (``sources'' in this example). The second level
names the object whose settings are being specified. Finally, the
third level specifies the custom default settings themselves. Note
that default values can be set to literal numbers, strings, or arrays
(denoted by comma-separated literals in square brackets).

The names of groups and objects come from the XML proxy definition
files in \ParaView's source code in the directory
\directory{ParaView/ParaViewCore/ServerManager/SMApplication/Resources}. The
group name is defined by the \texttt{name} attribute in a
\texttt{ProxyGroup} element. The object name comes from the
\texttt{name} attribute in the \texttt{Proxy} element (or elements of
\texttt{vtkSMProxy} subclasses). The property names come from the
\texttt{name} attribute in the \texttt{*Property} XML elements for the
object.

\begin{didyouknow}
The application-wide settings available in \paraview
through the \menu{Edit > Settings} menu are also saved to this user
settings file. Hence, if you have changed the application settings,
you will see some entries under a group named ``settings''.
\end{didyouknow}

\section{Configuring site-wide default settings}
\label{sec:ConfiguringDefaultSettingsSiteWide}

In addition to individual custom default settings, \ParaView offers a
way to specify site-wide custom default settings for a \ParaView
installation. These site-wide custom defaults must be defined in a
\JSON file with the same structure as the user settings file. In fact,
one way to create a site settings file is to set the custom defaults
desired in \paraview, close the program, and then copy the
user settings file to the site settings file. The site settings file
must be named \directory{ParaView-SiteSettings.json}.

The \ParaView executables will search for the site settings file in
several locations. If you installed \ParaView in the directory
\texttt{INSTALL}, then the \ParaView executables will search for the
site settings file in these directories in the specified order:

\begin{itemize}
\item \directory{INSTALL/share/paraview-X.Y}
\item \directory{INSTALL/lib}
\item \directory{INSTALL}
\item \directory{INSTALL/..}
\end{itemize}

where \texttt{X} is \ParaView's major version number and \texttt{Y} is
the minor version number. \ParaView executables will search these
directories in the given order, reading in the first
\directory{ParaView-SiteSettings.json} file it finds. The conventional
location for this kind of configuration file is in the \directory{share}
directory (the first directory searched), so we recommend placing the
site settings file there.

Custom defaults in the user settings file take precedence over custom
defaults in the site settings. If the same default is specified in
both the \directory{ParaView-SiteSettings.json} file and
\directory{ParaView-UserSettings.json} file in a user's directory, the
default specified in the \directory{ParaView-UserSettings.json} file will
be used. This is true for both object property settings and
application-settings set through the \menu{Edit > Settings} menu.

To aid in debugging problems with the site settings file location, you
can define an evironment variable named \texttt{PV\_SETTINGS\_DEBUG}
to something other than an empty string. This will turn on verbose
output showing where the \ParaView executables are looking for the site
settings file.
